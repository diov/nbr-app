import { Text } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';

import AuthScreen from './src/screens/Auth/Auth';
import DashboardScreen from './src/screens/Dashboard/Dashboard';
import SendScreen from './src/screens/Send/Send';
import TransactionsScreen from './src/screens/Transactions/Transactions';
import TransactionsAllScreen from './src/screens/Transactions/All';
import TransactionsSentScreen from './src/screens/Transactions/Sent';
import TransactionsReceivedScreen from './src/screens/Transactions/Received';
import TransactionDetails from './src/screens/Transactions/Details'
import ContactsScreen from './src/screens/Contacts/Contacts'
import ContactsAddModalScreen from './src/screens/Contacts/ContactsAddModal'

const store = configureStore();

Text.defaultProps.style = { fontFamily: 'OpenSans-Regular' }

//Register screens
Navigation.registerComponent('nbrapp.AuthScreen', () => AuthScreen, store, Provider);
Navigation.registerComponent('nbrapp.Dashboard', () => DashboardScreen, store, Provider);
Navigation.registerComponent('nbrapp.Send', () => SendScreen);
Navigation.registerComponent('nbrapp.Transactions', () => TransactionsScreen);
Navigation.registerComponent('nbrapp.Transactions.All', () => TransactionsAllScreen, store, Provider);
Navigation.registerComponent('nbrapp.Transactions.Sent', () => TransactionsSentScreen, store, Provider);
Navigation.registerComponent('nbrapp.Transactions.Received', () => TransactionsReceivedScreen, store, Provider);
Navigation.registerComponent('nbrapp.Transactions.Details', () => TransactionDetails, store, Provider);
Navigation.registerComponent('nbrapp.Contacts', () => ContactsScreen);
Navigation.registerComponent('nbrapp.Contacts.AddModal', () => ContactsAddModalScreen);

//Start the app
Navigation.startSingleScreenApp({
  screen: {
    screen: 'nbrapp.AuthScreen'
  }
});

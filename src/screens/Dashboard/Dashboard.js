import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text } from 'react-native';
import { connect } from 'react-redux';


import WalletStats from '../../components/screens/Dashboard/WalletStats';

class DashboardScreen extends Component {

    constructor(props) {
        super(props);

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    }

    onNavigatorEvent = event => {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'menuToggle') {
                this.props.navigator.toggleDrawer({
                    side: 'left'
                });
            }
        }
    }

    render () {
        return (
            <ScrollView style={{flex: 1}}>
                <View style={styles.container}>
                    <WalletStats />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center'
    }
});

const mapStateToProps = state => { 

};

export default DashboardScreen;
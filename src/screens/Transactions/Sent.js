import React, { Component } from 'react';
import { StyleSheet, ScrollView, FlatList } from 'react-native';
import { connect } from 'react-redux';

import TransactionItem from '../../components/screens/Transactions/TransactionItem';

class TransactionsSentScreen extends Component {

    openModal = (transactionId) => {
        this.props.navigator.showModal({
            screen: "nbrapp.Transactions.Details",
            title: "Detalhes",
            passProps: {
                transactionId
            },
            animationType: 'slide-up'
        });
    }

    _keyExtractor = (item) => item.id;

    render () {
        return (
            <ScrollView>
                <FlatList 
                    data={this.props.transactions}
                    keyExtractor={this._keyExtractor}
                    renderItem={transactions => (
                        <TransactionItem transaction={transactions.item} onTouch={this.openModal} />
                    )}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    dashboardContainer: {
        flex: 1
    }
});

const mapStateToProps = state => {
    return {
        transactions: state.transactions.transactions.filter(transaction => transaction.to.amount < 0)
    }
}

export default connect(mapStateToProps, null)(TransactionsSentScreen);
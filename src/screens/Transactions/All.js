import React, { Component } from 'react';
import { StyleSheet, ScrollView, FlatList, View, Text } from 'react-native';
import { connect } from 'react-redux';

import { fetchAllTransactions } from '../../store/actions/index';

import TransactionItem from '../../components/screens/Transactions/TransactionItem';

class TransactionsAllScreen extends Component {

    componentDidMount () {
        this.props.onLoad();
    }

    openModal = (transactionId) => {
        this.props.navigator.showModal({
            screen: "nbrapp.Transactions.Details",
            title: "Detalhes",
            passProps: {
                transactionId
            },
            animationType: 'slide-up'
        });
    }

    _keyExtractor = (item) => item.id;

    render () {
        return (
            <ScrollView>
                <FlatList 
                    data={this.props.transactions}
                    keyExtractor={this._keyExtractor}
                    renderItem={transactions => (
                        <TransactionItem transaction={transactions.item} onTouch={this.openModal} />
                    )}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    dashboardContainer: {
        flex: 1
    }
});

const mapStateToProps = state => {
    return {
        transactions: state.transactions.transactions
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onLoad: () => dispatch(fetchAllTransactions())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsAllScreen);
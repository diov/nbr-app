import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const startApp = () => {

    Promise.all([
        Icon.getImageSource('ios-home', 30),
        Icon.getImageSource('ios-paper-plane', 30),
        Icon.getImageSource('ios-filing-outline', 30),
        Icon.getImageSource('ios-book-outline', 30)
    ])
    .then(assets => {

        Navigation.startTabBasedApp({
            tabs: [
              {
                label: 'Dashboard', // tab label as appears under the icon in iOS (optional)
                screen: 'nbrapp.Dashboard', // unique ID registered with Navigation.registerScreen
                icon: assets[0], // local image asset for the tab icon unselected state (optional on iOS)
                iconInsets: { // add this to change icon position (optional, iOS only).
                  top: 6, // optional, default is 0.
                  left: 0, // optional, default is 0.
                  bottom: -6, // optional, default is 0.
                  right: 0 // optional, default is 0.
                },
                title: 'Dashboard', // title of the screen as appears in the nav bar (optional)
              },
              {
                label: 'Send', // tab label as appears under the icon in iOS (optional)
                screen: 'nbrapp.Send', // unique ID registered with Navigation.registerScreen
                icon: assets[1], // local image asset for the tab icon unselected state (optional on iOS)
                iconInsets: { // add this to change icon position (optional, iOS only).
                  top: 6, // optional, default is 0.
                  left: 0, // optional, default is 0.
                  bottom: -6, // optional, default is 0.
                  right: 0 // optional, default is 0.
                },
                title: 'Send', // title of the screen as appears in the nav bar (optional)
              },
              {
                label: 'Transactions', // tab label as appears under the icon in iOS (optional)
                screen: 'nbrapp.Transactions', // unique ID registered with Navigation.registerScreen
                icon: assets[2], // local image asset for the tab icon unselected state (optional on iOS)
                iconInsets: { // add this to change icon position (optional, iOS only).
                  top: 6, // optional, default is 0.
                  left: 0, // optional, default is 0.
                  bottom: -6, // optional, default is 0.
                  right: 0 // optional, default is 0.
                },
                title: 'Transactions', // title of the screen as appears in the nav bar (optional)
                topTabs: [{
                  screenId: 'nbrapp.Transactions.All',
                  title: 'All',
                }, {
                  screenId: 'nbrapp.Transactions.Sent',
                  title: 'Sent',
                },
                {
                  screenId: 'nbrapp.Transactions.Received',
                  title: 'Received',
                }]
              },
              {
                label: 'Contacts', // tab label as appears under the icon in iOS (optional)
                screen: 'nbrapp.Contacts', // unique ID registered with Navigation.registerScreen
                icon: assets[3], // local image asset for the tab icon unselected state (optional on iOS)
                iconInsets: { // add this to change icon position (optional, iOS only).
                  top: 6, // optional, default is 0.
                  left: 0, // optional, default is 0.
                  bottom: -6, // optional, default is 0.
                  right: 0 // optional, default is 0.
                },
                title: 'Contacts', // title of the screen as appears in the nav bar (optional)
              },
            ],
            tabsStyle: { //ios
              tabBarSelectedButtonColor: '#27ae60'
            },
            appStyle: { //android
              navBarBackgroundColor: '#27ae60',
              navBarTextColor: 'white',
              tabBarSelectedButtonColor: '#27ae60'
            }
            
          });

    });
}

export default startApp;
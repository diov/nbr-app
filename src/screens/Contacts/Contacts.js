import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text } from 'react-native';
import { connect } from 'react-redux';

class ContactsScreen extends Component {

    constructor(props) {
        super(props);

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    }

    static navigatorButtons = {
        rightButtons: [
          {
            title: 'Add contact',
            id: 'addContact',
          }
        ]
    };

    onNavigatorEvent = event => {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'addContact') {
                this.props.navigator.showLightBox({
                    screen: "nbrapp.Contacts.AddModal",
                    style: {
                      backgroundBlur: "dark",
                      backgroundColor: "#00000080",
                      tapBackgroundToDismiss: true
                    },
                });
            }
        }
    }

    render () {
        return (
            <ScrollView style={styles.dashContainer}>
                <Text> ContactsScreen </Text>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    dashboardContainer: {
        flex: 1
    }
});

const mapStateToProps = state => { 

};

export default ContactsScreen;
import React, { Component } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import { connect } from 'react-redux';

class ContactsAddModalScreen extends Component {

    state = {
        address: null,
        name: null,
        paymentId: null,
        anonymity: null,
        amount: null
    }

    render () {
        return (
                <View style={{backgroundColor: 'white', height: '80%', width: '90%'}}>
                    <TextInput
                        placeholder="Address"
                        placeholderTextColor="#218c74"
                        style={styles.input}
                        onChangeText={address => this.setState({address})}
                        value={this.state.address}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                    />
                    <TextInput
                        placeholder="Name"
                        placeholderTextColor="#218c74"
                        style={styles.input}
                        onChangeText={name => this.setState({name})}
                        value={this.state.name}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                    />
                    <TextInput
                        placeholder="PaymentId"
                        placeholderTextColor="#218c74"
                        style={styles.input}
                        onChangeText={paymentId => this.setState({paymentId})}
                        value={this.state.paymentId}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                    />
                </View>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        width: 220,
        height: 42,
        paddingLeft: 10,
        marginBottom: 10,
        color: '#27ae60',
        backgroundColor: '#f6f8fa',
    }
});

export default ContactsAddModalScreen;
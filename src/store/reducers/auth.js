import { AUTH_UPDATE_DATA } from '../actions/actionTypes';

const initialState = {
    email: '',
    id: '',
    name: '',
    role: '',
    token: ''
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case AUTH_UPDATE_DATA:
            return {
                email: action.authData.email,
                id: action.authData.id,
                name: action.authData.name,
                role: action.authData.role,
                token: action.authData.token
            }
        default:
            return state;
    }
}

export default reducer;
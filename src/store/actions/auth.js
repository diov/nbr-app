import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { AUTH_UPDATE_DATA } from './actionTypes';
import startApp from '../../screens/startApp';

export const tryAuth = (formData) => {
    return dispatch => {
        console.log(formData);
        axios.post('https://wallet.niobiocash.money/api/v1/users/auth', {
            email: formData.email,
            password: formData.password,
            twoFactorAuthToken: formData.twoFactorAuthToken.toString()
        })
        .then(({data}) => {
            updateAuthDataStorage(data);
            dispatch(updateAuthData(data));
            startApp();
        })
        .catch(error => {console.log(error); alert('Falha de conexão! Tente novamente mais tarde, verifique se você está conectado a internet.')});
    }
}

export const updateToken = oldToken => {
    return dispatch => {

        axios.post('https://wallet.niobiocash.money/api/v1/users/auth/tokens', {
            currentUser: oldToken,
        }, {
            headers: {
                Authorization: `Bearer ${oldToken}`
            }
        })
        .then(({data}) => {
            updateAuthDataStorage(data);
            dispatch(updateAuthData(data));
            startApp();
        })
        .catch(error => {
            dispatch(clearAuthData());
            alert('Falha ao buscar sua sessão, faça login novamente.')});
    }
}

export const updateAuthData = (authData) => {
    return {
        type: AUTH_UPDATE_DATA,
        authData
    };
}

export const updateAuthDataStorage = (authData) => {
    const now = new Date();
    const expirationDate = new Date(now.getTime() + 3600 * 1000);
    AsyncStorage.setItem('@nbr:auth:email', authData.email),
    AsyncStorage.setItem('@nbr:auth:id', authData.id),
    AsyncStorage.setItem('@nbr:auth:name', authData.name),
    AsyncStorage.setItem('@nbr:auth:role', authData.role),
    AsyncStorage.setItem('@nbr:auth:token', authData.token),
    AsyncStorage.setItem('@nbr:auth:expirationDate', expirationDate)
}

export const getTokenFromStorage = () => AsyncStorage.getItem('@nbr:auth:token');
export const getExpirationDateFromStorage = () => AsyncStorage.getItem('@nbr:auth:expirationDate');

export const clearAuthData = () => {
    AsyncStorage.removeItem('@nbr:auth:email');
    AsyncStorage.removeItem('@nbr:auth:id');
    AsyncStorage.removeItem('@nbr:auth:name');
    AsyncStorage.removeItem('@nbr:auth:role');
    AsyncStorage.removeItem('@nbr:auth:token');
    AsyncStorage.removeItem('@nbr:auth:expirationDate');
}

export const tryAutoLogin = () => {
    return async dispatch => {
            const tokenFromStorage = await getTokenFromStorage();
            if (!tokenFromStorage) return
            const expirationDate = await getExpirationDateFromStorage();
            const now = new Date();
            if (now >= Date.parse(expirationDate)) {
                dispatch(clearAuthData());
                return
            } else {
                dispatch(updateToken(tokenFromStorage));
                return
            }

    }

}

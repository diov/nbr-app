import { TRANSACTIONS_STORE_ALL } from './actionTypes';

import axios from 'axios';

export const storeTransactions = (transactions) => {
    return {
        type: TRANSACTIONS_STORE_ALL,
        transactions
    };
}

export const fetchAllTransactions = () => {
    return (dispatch, getState) => {
        axios.get('https://wallet.niobiocash.money/api/v1/transactions', {
            headers: {
                Authorization: `Bearer ${getState().auth.token}`
            }
        })
        .then(({data}) => {
            dispatch(storeTransactions(data));
        })
        .catch(error => alert('Problema ao buscar as transações' + error));
    }
}

import React, { Component } from 'react';
import { Text } from 'react-native';
import axios from 'axios';

import { precision } from './util';

class ConvertNbr extends Component {

    config = {
        symbol: 'NBR',
        name: 'Nióbio Cash',
        decimals: 8,
        defaultUnit: 100000000
    }

    convert = (from, to, amount) => {
        return axios.post('https://wallet-qa.niobiocash.money/api/v1/conversions', {
            ticker: {
                from,
                to
            },
            amount
        })
        .then(({ data }) => data.price.toFixed(this.props.decimals || precision(data.price)))
    }

    componentDidUpdate(prevProps, prevState) {
        this.convert(this.config.symbol, this.props.to, this.props.amount / this.config.defaultUnit)
        .then(convertedValue => this.setState({value: convertedValue}))
        .catch(error => console.log(`Problem trying to convert ${from} to ${to}.`, error));
    }

    state = {
        value: 0
    }
    
    render () {
        return <Text> {this.state.value} </Text>
    }
}

export default ConvertNbr;